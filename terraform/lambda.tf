# TODO : Create the lambda role with aws_iam_role

resource "aws_iam_role" "iam_for_lambda" {
  name = "iam_for_lambda"
  assume_role_policy = file("static/lambdaPolicy.json")
}

# TODO : Create lambda function with aws_lambda_function

resource "aws_lambda_function" "test_lambda" {
    filename      = "empty_lambda_code.zip"
    function_name = var.data_processing_lambda_lambda_name
    role          = aws_iam_role.iam_for_lambda.arn
    handler       = "lambda_main_app.lambda_handler"
    runtime       = "python3.7"
}

# TODO : Create a aws_iam_policy for the logging, this resource policy will be attached to the lambda role

resource "aws_iam_policy" "lambda_logging" {
    name        = "lambda_logging"
    path        = "/"
    description = "IAM policy for logging from a lambda"

    policy = file("static/loggingPolicy.json")
}


# TODO : Attach the logging policy to the lamda role with aws_iam_role_policy_attachment

resource "aws_iam_role_policy_attachment" "lambda_logs" {
    role       = aws_iam_role.iam_for_lambda.name
    policy_arn = aws_iam_policy.lambda_logging.arn
}

# TODO : Attach the AmazonS3FullAccess policy to the lambda role with aws_iam_role_policy_attachment

resource "aws_iam_role_policy_attachment" "AmazonS3FullAccess" {
    role = aws_iam_role.iam_for_lambda.name
    policy_arn = var.s3_full_access_policy_arn
}


# TODO : Allow the lambda to be triggered by a s3 event with aws_lambda_permission

resource "aws_lambda_permission" "allow_bucket" {
    statement_id  = "AllowFromS3"
    action        = "lambda:InvokeFunction"
    function_name = aws_lambda_function.test_lambda.function_name
    principal     = "s3.amazonaws.com"
    source_arn    = aws_s3_bucket.s3-job-offer-bucket-dureau-vinaschi-beauvois.arn
}








